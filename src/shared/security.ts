export type Severity = 'unknown' | 'safe' | 'warning' | 'danger' | 'extreme';
export type ThreatAssessmentContext = 'all' | 'currentFrame' | 'currentTab' | 'excludingCurrentTab';

export interface URLThreatAssessment {
    url: string;
    severity: Severity;
    occurances: number;
}

export interface ThreatAssessment {
    urls: URLThreatAssessment[];
}

export interface UrlThreatAssessment {
    url: string;
    severity: Severity;
    occurances: number;
}

