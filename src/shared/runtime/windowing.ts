
export interface SenderIdentity {
    extensionID: string;
    frameID: number;
    tabID: number;
    windowID: number;
}
