/// <reference types="react-scripts" />

declare namespace NodeJS {
    interface ProcessEnv {
        // Create React App specific
        readonly BROWSER?: string;
        readonly BROWSER_ARGS?: string;
        readonly CHOKIDAR_USEPOLLING?: boolean;
        readonly CI?: boolean;
        readonly EXTEND_ESLINT?: boolean;
        readonly GENERATE_SOURCEMAP?: boolean;
        readonly HOST?: string;
        readonly HTTPS?: boolean;
        readonly IMAGE_INLINE_SIZE_LIMIT?: number;
        readonly INLINE_RUNTIME_CHUNK?: boolean;
        readonly NODE_ENV: 'development' | 'production' | 'test';
        readonly NODE_PATH?: string;
        readonly PORT?: number;
        readonly PUBLIC_URL: string;
        readonly REACT_EDITOR?: string;

        // App specific
        readonly REACT_APP_NAME: string;
    }
}