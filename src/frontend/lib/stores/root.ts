import { UIStore } from './ui';

export interface IStores {
    ui: UIStore;
}

export const STORES: IStores = { ui: new UIStore() };
