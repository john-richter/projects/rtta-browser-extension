import { Theme, createMuiTheme } from '@material-ui/core';
import * as colors from '@material-ui/core/colors';
import { CreatePalette, CreateTypopgraphy } from './create';

export type TThemeVariant = 'light' | 'dark';

export const LIGHT_THEME: Theme = createMuiTheme({
    palette: CreatePalette({
        type: 'light',
        tooltip: { main: colors.grey.A100 },
        info: { main: colors.lightBlue[500] },
        success: { main: colors.green[500] },
        warning: { main: colors.amber[500] },
    }),
    typography: CreateTypopgraphy
});

export const DARK_THEME: Theme = createMuiTheme({
    palette: CreatePalette({
        type: 'dark',
        tooltip: { main: colors.grey.A700 },
        info: { main: colors.lightBlue[500] },
        success: { main: colors.green[500] },
        warning: { main: colors.amber[500] },
    }),
    typography: CreateTypopgraphy
});

