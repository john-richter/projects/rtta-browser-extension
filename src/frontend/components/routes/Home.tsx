import * as React from 'react';
import { TReactFCR } from '../../typings/utils';
import { Paper, TableRow, Table, TableCell, TableHead, TableBody, Grid, Typography, Theme } from '@material-ui/core';
import { ThreatAssessment, URLThreatAssessment, Severity } from '../../../shared/security';
import { useThreatAssessment } from '../../hooks/useThreatAssessment';
import { MakeStyles, makeStyles, UseStyles } from '../../typings/mui';

type ClassKey = 'stats' | 'table';
const useStyles: MakeStyles<ClassKey> = makeStyles<ClassKey>((theme: Theme) => ({
    stats: { padding: theme.spacing(2), margin: theme.spacing(1) },
    table: { overflow: 'auto', maxWidth: '100%' }
}));

export interface IHomeP { }

export function Home(): TReactFCR {
    const assessment: ThreatAssessment = useThreatAssessment();
    const classes: UseStyles<ClassKey> = useStyles();

    const urlSeverities: Map<Severity, URLThreatAssessment[]> = new Map([
        ['unknown', []], ['safe', []], ['warning', []], ['danger', []], ['extreme', []]
    ]);
    for (const url of assessment.urls) {
        if (!url.severity) {
            urlSeverities.get('unknown')!.push(url);
        }
        if (urlSeverities.has(url.severity)) {
            urlSeverities.get(url.severity)!.push(url);
        }
    }
    return (
        <div>
            <Paper className={classes.stats}>
                <Grid container alignItems='baseline' justify='space-between'>
                    <Grid item xs>
                        <Typography variant='h4' align='center'>{urlSeverities.get('extreme')!.length}</Typography>
                        <Typography variant='subtitle1' align='center' color='textSecondary'>extreme</Typography>
                    </Grid>
                    <Grid item xs>
                        <Typography variant='h4' align='center'>{urlSeverities.get('danger')!.length}</Typography>
                        <Typography variant='subtitle1' align='center' color='textSecondary'>danger</Typography>
                    </Grid>
                    <Grid item xs>
                        <Typography variant='h4' align='center'>{urlSeverities.get('warning')!.length}</Typography>
                        <Typography variant='subtitle1' align='center' color='textSecondary'>warning</Typography>
                    </Grid>
                    <Grid item xs>
                        <Typography variant='h4' align='center'>{urlSeverities.get('safe')!.length}</Typography>
                        <Typography variant='subtitle1' align='center' color='textSecondary'>safe</Typography>
                    </Grid>
                    <Grid item xs>
                        <Typography variant='h4' align='center'>{urlSeverities.get('unknown')!.length}</Typography>
                        <Typography variant='subtitle1' align='center' color='textSecondary'>unknown</Typography>
                    </Grid>
                </Grid>
            </Paper>
            <Paper className={classes.table}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>URL</TableCell>
                            <TableCell>Severity</TableCell>
                            <TableCell align='right'>Occurrances</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {assessment.urls.map((ta: URLThreatAssessment) => (
                            <TableRow key={ta.url}>
                                <TableCell>{ta.url}</TableCell>
                                <TableCell>{ta.severity}</TableCell>
                                <TableCell align='right'>{ta.occurances}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Paper>
        </div>
    );
}
