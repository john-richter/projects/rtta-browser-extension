import * as React from 'react';
import { CssBaseline as MUICssBaseline } from '@material-ui/core';
import { Fonts } from './Fonts';
import { TReactFCR, TReactFCP } from '../../typings/utils';
import { Meta } from './Meta';
import { Providers } from './Providers';

export interface IBaselineP {
}

export function Baseline(props: TReactFCP<IBaselineP>): TReactFCR {
    return (
        <Providers>
            <MUICssBaseline />
            <Fonts />
            <Meta />
            {props.children}
        </Providers>
    );
}
