import * as React from 'react';
import { TReactFCR } from '../../typings/utils';
import { BrowserRouter, Route, Switch, HashRouter } from 'react-router-dom';
import App from './App';
import { Home } from '../routes/Home';

export function Renderer(): TReactFCR {
    return (
        <App>
            <HashRouter>
                <Switch>
                    <Route exact path='/' component={Home} />
                    <Route render={() => <div>ruh roh</div>} />
                </Switch>
            </HashRouter>
        </App>
    );
}
