import * as React from 'react';
import { Helmet } from 'react-helmet';
import { TReactFCR } from '../../typings/utils';

export function Meta(): TReactFCR {
    return (
        <Helmet
            defaultTitle={process.env.REACT_APP_NAME}
            titleTemplate={`${process.env.REACT_APP_NAME} | %s`}
        />
    );
}
