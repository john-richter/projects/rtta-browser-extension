import * as React from 'react';
import { TReactFCR, TReactFCP } from '../../typings/utils';
import { StylesProvider } from '@material-ui/styles';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/styles';
import { StoresProvider } from '../../lib/stores/provider';
import { STORES, IStores } from '../../lib/stores/root';

export interface IProvidersP { }

export function Providers(props: TReactFCP<IProvidersP>): TReactFCR {
    const { ui }: IStores = STORES;
    return (
        <StoresProvider value={STORES}>
            <StylesProvider jss={ui.jss} generateClassName={ui.jssClassNameGenerator}>
                <MuiThemeProvider theme={ui.theme}>
                    {props.children}
                </MuiThemeProvider>
            </StylesProvider>
        </StoresProvider>
    );
}
