import React from 'react';
import { TReactFCR, TReactFCP } from '../../typings/utils';
import { makeStyles, MakeStyles, UseStyles } from '../../typings/mui';

const useStyles: MakeStyles<'root'> = makeStyles<'root'>({
    root: { width: '800px', height: '600px', overflow: 'auto' }
});

function App(props: TReactFCP): TReactFCR {
    const classes: UseStyles<'root'> = useStyles();
    return <div className={classes.root}>{props.children}</div>;
}

export default App;
