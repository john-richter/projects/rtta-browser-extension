import './config/init'
import React from 'react';
import ReactDOM from 'react-dom';
import { Baseline } from './components/app/Baseline';
import { Renderer } from './components/app/Renderer';

ReactDOM.render(
    <Baseline><Renderer /></Baseline>,
    document.getElementById('root')
);
