import { observable, action, ObservableMap } from 'mobx';

export class ChromeNavigation {
    @observable private _activeTabIDs: ObservableMap<number, number> = observable.map();

    constructor() {
        chrome.windows.onCreated.addListener(this._handleWindowCreated)
        chrome.tabs.onActivated.addListener(this._handleActiveTabChange);
    }

    @action private _handleActiveTabChange = (activeInfo: chrome.tabs.TabActiveInfo): void => {
        this._activeTabIDs.set(activeInfo.windowId, activeInfo.tabId);
    };

    @action private _handleWindowCreated = (
        window: chrome.windows.Window, filters?: chrome.windows.WindowEventFilter
    ) => {
        const activeTab: chrome.tabs.Tab | undefined = (window.tabs || []).find((t: chrome.tabs.Tab) => t.active);
        this._activeTabIDs.set(
            window.id,
            (activeTab && activeTab.id !== undefined) ? activeTab.id : chrome.tabs.TAB_ID_NONE
        );
    };

    public activeTabInWindow(windowID: number): number {
        console.log(`windowID: ${windowID}`);
        console.log(JSON.stringify(this._activeTabIDs.toJSON()));
        const tabID: number | undefined = this._activeTabIDs.get(windowID);
        return tabID !== undefined ? tabID : chrome.tabs.TAB_ID_NONE;
    }
}

export const CHROME_NAVIGATION_STORE: ChromeNavigation = new ChromeNavigation();
