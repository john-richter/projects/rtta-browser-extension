import { observable, IObservableArray, action, ObservableMap } from 'mobx';
import { MessageSender } from '../messageSender';

export class MessageSenderStore {
    @observable private _extension: IObservableArray<MessageSender> = observable.array();
    @observable private _windows: IObservableArray<MessageSender> = observable.array();
    @observable private _tabs: ObservableMap<number, MessageSender[]> = observable.map();

    @action.bound public addSender(sender: MessageSender): void {
        switch (sender.senderType) {
            case 'extension':
                this._extension.push(sender); // We have no way to determine the sender's real identity to dedupe
                break;
            case 'window':
                if (!this._windows.some((s: MessageSender) => s.sameWindow(sender))) {
                    this._windows.push(sender);
                }
                break;
            case 'tab':
                const frameSenders: MessageSender[] | undefined = this._tabs.get(sender.tabID);
                if (frameSenders !== undefined) {
                    if (!frameSenders.some((s: MessageSender) => s.sameFrame(sender))) {
                        frameSenders.push(sender);
                    }
                } else {
                    this._tabs.set(sender.tabID, [sender]);
                }
                break;
            default:
                break;
        }
    }
}

export const BACKEND_CLIENT_STORE: MessageSenderStore = new MessageSenderStore();
