import { observable, computed } from 'mobx';
import { SenderIdentity } from '../../shared/runtime/windowing';
import { EXTENSION_ID } from '../../shared/runtime/identifiers';

export type MessageSenderType = 'extension' | 'window' | 'tab';

export class MessageSender {
    public static fromChromeMessageSender(sender: chrome.runtime.MessageSender): MessageSender {
        const identity: SenderIdentity = {
            extensionID: sender.id || 'unknown',
            windowID: -1,
            tabID: chrome.tabs.TAB_ID_NONE,
            frameID: -1
        }
        if (sender.tab) {
            identity.windowID = sender.tab.windowId;
            identity.tabID = sender.tab.id !== undefined ? sender.tab.id : chrome.tabs.TAB_ID_NONE;
            identity.frameID = sender.frameId !== undefined ? sender.frameId : -1; // Only set when `sender.tab` is set
        }
        return new MessageSender(identity);
    }

    @observable private _extensionID: string;
    @observable private _windowID: number;
    @observable private _tabID: number;
    @observable private _frameID: number;

    constructor(identity: SenderIdentity) {
        // Order of importance window > tabID > tabIndex > frameID > sessionID
        // tabID can equal sessionID or chrome.tabs.NONE in some circumstances.
        this._extensionID = identity.extensionID;
        this._windowID = identity.windowID;
        this._tabID = identity.tabID;
        this._frameID = identity.frameID;
    }

    public eq(other: MessageSender): boolean {
        if (this === other) return true;
        return this.sameWindow(other) && this.sameFrame(other);
    }

    public sameWindow(other: MessageSender): boolean {
        return this._windowID === other._windowID;
    }

    public sameTab(other: MessageSender): boolean {
        return this._tabID === other._tabID;
    }

    public sameFrame(other: MessageSender): boolean {
        return this.sameTab(other) && this._frameID === other._frameID;
    }

    @computed public get id(): string {
        return `${this._windowID}::${this._tabID}::${this._frameID}`
    }

    @computed public get isValid(): boolean {
        return this._extensionID === EXTENSION_ID; // WindowID is undefined when the popup is the sender
    }

    @computed public get windowID(): number {
        return this._windowID;
    }

    @computed public get tabID(): number {
        return this._tabID;
    }

    @computed public get frameID(): number {
        return this._frameID;
    }

    @computed public get isTab(): boolean {
        return this.tabID !== chrome.tabs.TAB_ID_NONE;
    }

    @computed public get senderType(): MessageSenderType {
        return this.tabID === chrome.tabs.TAB_ID_NONE ? 'window' : 'tab';
    }
}
