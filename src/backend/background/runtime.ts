import { Message, ChromeSendResposeFn } from '../../shared/runtime/messages';

export interface ChromeRuntimeLifecycle {
    /**
     * Fired when the extension is first installed, when the extension is updated to a new version, and when Chrome
     * is updated to a new version.
     */
    onInstalled?(details: chrome.runtime.InstalledDetails): void;

    /**
     * Fired when a profile that has this extension installed first starts up. This event is not fired when an incognito
     * profile is started, even if this extension is operating in 'split' incognito mode.
     */
    onStartup?(): void;
    /**
     * Sent to the event page just before it is unloaded. This gives the extension opportunity to do some clean up.
     * Note that since the page is unloading, any asynchronous operations started while handling this event are not
     * guaranteed to complete. If more activity for the event page occurs before it gets unloaded the onSuspendCanceled
     * event will be sent and the page won't be unloaded.
     */
    onSuspend?(): void;

    /**
     * Sent after onSuspend to indicate that the app won't be unloaded after all.
     */
    onSuspendCancelled?(): void;

    /**
     * Fired when an update is available, but isn't installed immediately because the app is currently running. If you
     * do nothing, the update will be installed the next time the background page gets unloaded, if you want it to be
     * installed sooner you can explicitly call chrome.runtime.reload(). If your extension is using a persistent
     * background page, the background page of course never gets unloaded, so unless you call chrome.runtime.reload()
     * manually in response to this event the update will not get installed until the next time chrome itself restarts.
     * If no handlers are listening for this event, and your extension has a persistent background page, it behaves as
     * if chrome.runtime.reload() is called in response to this event.
     */
    onUpdateAvailable?(details: chrome.runtime.UpdateAvailableDetails): void;

    /**
     * Fired when a connection is made from either an extension process or a content script (by runtime.connect).
     */
    onConnect?(port: chrome.runtime.Port): void;

    /**
     * Fired when a connection is made from another extension (by runtime.connect).
     */
    onConnectExternal?(port: chrome.runtime.Port): void;

    /**
     * Fired when a message is sent from either an extension process (by runtime.sendMessage) or a content script (by
     * tabs.sendMessage).
     *
     * Must return if `sendResponse` was called since only one response per message may occur.
     */
    onMessage?(message: Message, sender: chrome.runtime.MessageSender, sendResponse: ChromeSendResposeFn): boolean;
}

export class ChromeRuntime {
    private _onInstalled: ChromeRuntimeLifecycle['onInstalled'][] = [];
    private _onStartup: ChromeRuntimeLifecycle['onStartup'][] = [];
    private _onSuspend: ChromeRuntimeLifecycle['onSuspend'][] = [];
    private _onSuspendCancelled: ChromeRuntimeLifecycle['onSuspendCancelled'][] = [];
    private _onUpdateAvailable: ChromeRuntimeLifecycle['onUpdateAvailable'][] = [];
    private _onConnect: ChromeRuntimeLifecycle['onConnect'][] = [];
    private _onConnectExternal: ChromeRuntimeLifecycle['onConnectExternal'][] = [];
    private _onMessage: ChromeRuntimeLifecycle['onMessage'][] = [];

    constructor(daemons: ChromeRuntimeLifecycle[]) {
        for (const d of daemons) this.registerDaemon(d);
    }

    public registerDaemon(daemon: ChromeRuntimeLifecycle): void {
        if (daemon.onInstalled) this._onInstalled.push(daemon.onInstalled);
        if (daemon.onStartup) this._onStartup.push(daemon.onStartup);
        if (daemon.onSuspend) this._onSuspend.push(daemon.onSuspend);
        if (daemon.onSuspendCancelled) this._onSuspendCancelled.push(daemon.onSuspendCancelled);
        if (daemon.onUpdateAvailable) this._onUpdateAvailable.push(daemon.onUpdateAvailable);
        if (daemon.onConnect) this._onConnect.push(daemon.onConnect);
        if (daemon.onConnectExternal) this._onConnectExternal.push(daemon.onConnectExternal);
        if (daemon.onMessage) this._onMessage.push(daemon.onMessage);
    }

    public start(): void {
        chrome.runtime.onInstalled.addListener((details: chrome.runtime.InstalledDetails): void => {
            for (const cb of this._onInstalled) {
                if (cb) cb(details);
            }
        });
        chrome.runtime.onStartup.addListener((): void => {
            for (const cb of this._onStartup) {
                if (cb) cb();
            }
        });
        chrome.runtime.onSuspend.addListener((): void => {
            for (const cb of this._onSuspend) {
                if (cb) cb();
            }
        });
        chrome.runtime.onSuspendCanceled.addListener((): void => {
            for (const cb of this._onSuspendCancelled) {
                if (cb) cb();
            }
        });
        chrome.runtime.onUpdateAvailable.addListener((details: chrome.runtime.UpdateAvailableDetails): void => {
            for (const cb of this._onUpdateAvailable) {
                if (cb) cb(details);
            }
        });
        chrome.runtime.onConnect.addListener((port: chrome.runtime.Port): void => {
            for (const cb of this._onConnect) {
                if (cb) cb(port);
            }
        });
        chrome.runtime.onConnectExternal.addListener((port: chrome.runtime.Port): void => {
            for (const cb of this._onConnectExternal) {
                if (cb) cb(port);
            }
        });
        chrome.runtime.onMessage.addListener(
            (message: Message, sender: chrome.runtime.MessageSender, sendResponse: ChromeSendResposeFn) => {
                for (const cb of this._onMessage) {
                    if (cb) return cb(message, sender, sendResponse);
                }
            }
        );
    }
}
