This project was created as an exploratory demo for [BreachTek Solutions](https://breachteksolutions.com/). [Read more](https://jrichter.io/projects/bts-extension/) about it.

The main goal is to have:
1. A basic extension for the major browsers
2. A icon button in the address bar that contains a numbered badge with the number of threats on the active page/tab
3. A webview that describes each link on the page and its threat level
4. (Optional) Injected tooltips over each link that display information about each link

## Chromium

**[Big changes are coming](https://docs.google.com/document/d/1nPu6Wy4LWR66EFLeYInl3NzzhHzc-qnk4w4PX-0XMw8/mobilebasic#)** that potentially prevent our ability to stop a request from happening. [Declarative Net Request](https://developer.chrome.com/extensions/declarativeNetRequest) is replacing it. We might be able to use its dynamic capabilities, but it wouldn't be as nice as the `Web Request` API.

### [APIs](https://developer.chrome.com/extensions/api_index)

#### [Content Settings](https://developer.chrome.com/extensions/contentSettings)

> Use the chrome.contentSettings API to change settings that control whether websites can use features such as cookies, JavaScript, and plugins. More generally speaking, content settings allow you to customize Chrome's behavior on a per-site basis instead of globally.

Versions: >=31
Permission: `contentSettings`

#### [Browser Action](https://developer.chrome.com/extensions/browserAction)

> Use browser actions to put icons in the main Google Chrome toolbar, to the right of the address bar. In addition to its icon, a browser action can have a tooltip, a badge, and a popup.

Versions: >=31
Permission: `browser_action`

#### [Commands](https://developer.chrome.com/extensions/commands)

> Use the commands API to add keyboard shortcuts that trigger actions in your extension, for example, an action to open the browser action or send a command to the extension.

Versions: >=31
Permission: `commands`

#### [Context Menus](https://developer.chrome.com/extensions/contextMenus)

> Use the chrome.contextMenus API to add items to Google Chrome's context menu. You can choose what types of objects your context menu additions apply to, such as images, hyperlinks, and pages.

Versions: >=31
Permission: `contextMenus`

#### [Declarative Content](https://developer.chrome.com/extensions/declarativeContent)

> Use the chrome.declarativeContent API to take actions depending on the content of a page, without requiring permission to read the page's content.

Versions: >=33
Permission: `declarativeContent`

#### [Downloads](https://developer.chrome.com/extensions/downloads)

> Use the chrome.downloads API to programmatically initiate, monitor, manipulate, and search for downloads.

Versions: >=31
Permission: `downloads`

#### [Events](https://developer.chrome.com/extensions/events)

> The chrome.events namespace contains common types used by APIs dispatching events to notify you when something interesting happens.

Versions: >=31
Permission: N/A

#### [Extension](https://developer.chrome.com/extensions/extension)

> The chrome.extension API has utilities that can be used by any extension page. It includes support for exchanging messages between an extension and its content scripts or between extensions, as described in detail in Message Passing.

Versions: >=-31
Permission: N/A

#### [Extension Types](https://developer.chrome.com/extensions/extensionTypes)

> The chrome.extensionTypes API contains type declarations for Chrome extensions.

Versions: >=39
Permission: N/A

#### [Permissions](https://developer.chrome.com/extensions/permissions)

> Use the chrome.permissions API to request declared optional permissions at run time rather than install time, so users understand why the permissions are needed and grant only those that are necessary.

Versions: >=31
Permission: N/A

#### [Privacy](https://developer.chrome.com/extensions/privacy)

> Use the chrome.privacy API to control usage of the features in Chrome that can affect a user's privacy. This API relies on the ChromeSetting prototype of the type API for getting and setting Chrome's configuration.

Versions: >=31
Permission: `privacy`

#### [Runtime](https://developer.chrome.com/extensions/runtime)

> Use the chrome.runtime API to retrieve the background page, return details about the manifest, and listen for and respond to events in the app or extension lifecycle. You can also use this API to convert the relative path of URLs to fully-qualified URLs.

Versions: >=31
Permission: N/A

#### [Storage](https://developer.chrome.com/extensions/storage)

> Use the chrome.storage API to store, retrieve, and track changes to user data.

Versions: >=31
Permission: `storage`

#### [Tab Capture](https://developer.chrome.com/extensions/tabCapture)

> Use the chrome.tabCapture API to interact with tab media streams.

Versions: >=31
Permission: `tabCapture`

#### [Types](https://developer.chrome.com/extensions/types)

> The chrome.types API contains type declarations for Chrome.

Versions: >=31
Permission: N/A

#### [Web Navigation](https://developer.chrome.com/extensions/webNavigation)

> Use the chrome.webNavigation API to receive notifications about the status of navigation requests in-flight.

Versions: >=31
Permission: `webNavigation`

#### [Web Request](https://developer.chrome.com/extensions/webRequest)

> Use the chrome.webRequest API to observe and analyze traffic and to intercept, block, or modify requests in-flight.

Versions: >=31
Permission: `webRequest`

#### [Declarative Net Request](https://developer.chrome.com/extensions/declarativeNetRequest) **BETA**

> The chrome.declarativeNetRequest API is used to block or modify network requests by specifying declarative rules.

A replacement for the request blocking features of `Web Request`.

Versions: BETA
Permission: `declarativeNetRequest`

### Useful Example Projects from Google

`basic`: webrequest logging
`content_script`: dynamic element modification and css injection
`event_page`, `global_context_search`: right click context menus
`optional_permissions`: Requesting permissions dymanically and creating the "New Tab" view
`stylizr`: css injection
`water_alarm_notification`: options window tool tip

### Useful Information

The max dimensions of the popup window are 800px x 600px
